﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {

	public float btnX;
	public float btnY;
	public float btnW;
	public float btnH;

	public GameObject playerPrefab;
	public Transform spawnObject;

	private bool refreshing;
	private HostData[] hostData;

	private string gameName = "CGCookie_Tutorial_Networking_bpervan";

	void StartServer()
	{
		Network.InitializeServer (32, 25001, !Network.HavePublicAddress());
		MasterServer.RegisterHost (gameName, "Tutorial Game Name", "This is a tutorial game");
	}

	void RefreshHostList()
	{
		MasterServer.RequestHostList (gameName);
		refreshing = true;

		Debug.Log(MasterServer.PollHostList().Length);
	}


	void spawnPlayer()
	{
		Network.Instantiate (playerPrefab, spawnObject.position, Quaternion.identity, 0);
	}

	//Messages
	void OnServerInitialized()
	{
		Debug.Log ("Server initialized!");
		spawnPlayer ();
	}

	void OnConnectedToServer()
	{
		spawnPlayer ();
	}

	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if (mse == MasterServerEvent.RegistrationSucceeded) {
			Debug.Log("Registered Server!");		
		}
	}

	//GUI
	void OnGUI()
	{
		if(!Network.isClient && !Network.isServer){
			if (GUI.Button (new Rect (btnX, btnY, btnW, btnH), "Start Server")) 
			{
				Debug.Log ("Starting server");
				StartServer();
			}
			
			float newW = btnY * 1.2f + btnH;
			if (GUI.Button (new Rect (btnX, newW, btnW, btnH), "Join Server")) 
			{
				Debug.Log ("Refreshing");
				RefreshHostList();
			}
			float newX = btnX * 1.5f + btnW;
			float newY;
			float newW2 = btnW * 3f;
			float newH = btnH * 0.5f;
			int i;
			if (hostData != null) {
				for (i = 0; i < hostData.Length; ++i) {
					newY = btnY * 1.2f + (btnH * i);
					if(GUI.Button(new Rect(newX, newY, newW2, newH), hostData[i].gameName)){
						Network.Connect(hostData[i]);
					}
				}
			}
		}

	}

	// Use this for initialization
	void Start () {
		btnX = Screen.width * 0.05f;
		btnY = Screen.width * 0.05f;
		btnW = Screen.width * 0.1f;
		btnH = Screen.width * 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (refreshing) {
			if(MasterServer.PollHostList().Length != 0){
				refreshing = false;
				Debug.Log(MasterServer.PollHostList().Length);
				hostData = MasterServer.PollHostList();
			}		
		}
	}
}
