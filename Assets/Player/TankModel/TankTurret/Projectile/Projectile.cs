﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	private float velocity;
	private float damage;
	private bool moving;
	public Player owner;
	// Use this for initialization
	void Start () {
		velocity = 50;
		rigidbody.mass = 0.5f;
		damage = 10;

	}

	void FixedUpdate () {
		if (moving)
			move ();
	}

	private void move () {
		rigidbody.MovePosition (transform.position + transform.forward * velocity * Time.deltaTime);
	}

	public void fire () {
		moving = true;
	}

	void OnTriggerEnter (Collider other) {
		Debug.Log (other.gameObject.name);
		if (other.gameObject.GetComponentInParent<TankModel> () != null) {
			Debug.Log ("Tank model: " + other.gameObject.GetComponentInParent<TankModel> ().name);
		}

		Destroy (this.gameObject);
	}
}
