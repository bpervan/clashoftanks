﻿using UnityEngine;
using System.Collections;

public class TankModel : MonoBehaviour {

	public TankBody body;
	public TankTurret turret;
	public Player player;
	private bool moving, rotating;
	public float rotation, movement;

	void Start () {
		//body = GetComponentInChildren <TankBody > ();
		//turret = GetComponentInChildren <TankTurret> ();
		moving = false;
		rotating = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (moving)
			move ();
		if (rotating)
			rotateBody ();

	}

	private void move () {
		rigidbody.MovePosition (body.transform.position + body.transform.TransformDirection(0, 0, movement) * body.movementSpeed * Time.deltaTime);

	}

	private void rotateBody () {
		Vector3 angleVelocity = new Vector3 (0, rotation * body.bodyRotationSpeed, 0);
		Quaternion deltaRotation = Quaternion.Euler(angleVelocity* Time.deltaTime);
		rigidbody.MoveRotation(rigidbody.rotation * deltaRotation);
	}

	public void startMoving (float movement) {
		if (movement != 0) {
			moving = true;
			this.movement = movement;
		} else {
			moving = false;
		}
	}

	public void startRotating (float rotation) {
		if (rotation != 0) {
			this.rotation = rotation;
			rotating = true;
		} else {
			rotating = false;
		}
	}

	
}





